## HomeWork #3

##### Cсылка на видео
[Video](https://drive.google.com/open?id=1XKXKzcg6IVkRGyCo5tOUpWt0PwZ7B24U)

##### Ссылки на презентацию
[Presentation](https://docs.google.com/presentation/d/1wKTT9hBLtdyNWO04Nwc_sEF9E-1Iwk6XDTyq3FWDLHs/pub?start=false&loop=false&delayms=3000&slide=id.p)

##### Справочники
[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

[Деструктивное присваивание](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

[push](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/push)

[splice](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)

[some](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/some)

[every](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/every)

##### Стиль кода
https://learn.javascript.ru/coding-style

##### Ссылки из презентации
[charCodeAt](https://www.w3schools.com/jsref/jsref_charCodeAt.asp)
