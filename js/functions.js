// Функция

// --------------------------------------------------------------------------------------------------------------
// 1. Создать функцию multiply, которая будет принимать любое количество чисел и возвращать их произведение:
//    multiply(1, 2, 3) = 6(1*2*3)
//    Если нет ни одного аргумента, вернуть ноль: multiply() = 0
// --------------------------------------------------------------------------------------------------------------
function multiply() {
    if(!arguments.length) return 0;

    let total = 1;

    for (let i = 0; i < arguments.length; i++) {
        let number = parseFloat(arguments[i]);

        if (isNaN(number)) {
            return new Error('Аругмент должен быть числом')
        } else {
            total *= number;
        }
    }

    return total;
}

console.log(multiply(1, 2, 3)); // 6
console.log(multiply()); // 0
console.log(multiply('5str', 1, 2)); // 10
console.log(multiply('str5', 1, 2)); // Аругмент должен быть числом

// --------------------------------------------------------------------------------------------------------------
// 3. Создать функцию, которая принимает строку и возвращает строку-перевертыш: reverseString("test") = "tset".
// --------------------------------------------------------------------------------------------------------------
function reverseString(str) {
    if (!str.length) return new Error('Не передана строка');
    if (typeof(str) !== 'string') return new Error('Аргумент не является строкой');

    let newString = '';

    for (let i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }

    return newString;
}

console.log(reverseString("test")); // tset
console.log(reverseString()); // Error: Не передана строка
console.log(reverseString(1231231)); // Error: Аргумент не является строкой

// ---------------------------------------------------23-----------------------------------------------------------
// 4. Создать функцию, которая в качестве аргумента принимает строку из букв и возвращает строку,
//    где каждый символ разделен пробелом и заменен на юникод-начение символа:
//    getCodeStringFromText('hello') = "104 101 108 108 111"
//    подсказка: для получения кода используйте специальный метод charCodeAt();
// --------------------------------------------------------------------------------------------------------------
function getCodeStringFromText(str) {
    if (str === undefined) return new Error('Не передана строка');
    if (typeof(str) !== 'string') return new Error('Аргумент не является строкой');

    let newString = '';

    for (let i = 0; i < str.length; i++) {
        let space = i < str.length - 1 ? ' ' : '';

        newString += str.charCodeAt(i) + space;
    }

    return newString;
}

console.log(getCodeStringFromText('hello')); // 104 101 108 108 111
console.log(getCodeStringFromText()); // Error: Не передана строка
console.log(getCodeStringFromText(null)); // Error: Аргумент не является строкой

// --------------------------------------------------------------------------------------------------------------
// 6. Создать две функции и дать им осмысленные названия:
//    - первая функция принимает массив и колбэк
//    - вторая функция (колбэк) обрабатывает каждый элемент массива
//    Первая функция возвращает строку "New value:" и обработанный массив:
//
//    firstFunc(['my', 'name', 'is', 'Trinity'], secondFunc) -> "New value: MyNameIsTrinity"
//    firstFunc(['10', '20', '30'], secondFunc) -> "New value: 100, 200, 300,"
//    firstFunc([{age: 45, name: 'Jhon'}, {age: 20, name: 'Aaron'}], secondFunc) -> "New value: Jhon is 45, Aaron is 20,"
//    firstFunc(['abc', '123'], secondFunc) -> "New value: cba, 321" // строки инвертируются
//
//    firstFunc([1, 2, 3], function(number) { return number + 5 + ',';}); -> "New value: MyNameIsTrinity" // 'New value: 6, 7, 8,'
//
//    Подсказка: secondFunc должна быть представлена функцией, которая принимает один аргумент (каждый элемент массива)
//               и возвращает результат его обработки
// ------------------------------------------------------------------------//--------------------------------------
function showMessage(arr, handler) {
    if (!arr.length) return new Error('Не передан массив');

    let newString = 'New value: ';

    for (let i = 0; i < arr.length; i++) {
        newString += handler(arr[i]);
    }

    return newString;
}

function uppercaseFirstLetter(el) {
    if (typeof(el) !== 'string') return new Error('Аргумент не является строкой, ');

    let res = el[0].toUpperCase();

    for (let i = 1; i < el.length; i++) {
        res += el[i];
    }

    return res;
}

// or with method slice()
// let concatElements = element => element[0].toUpperCase() + element.slice(1);

function increaseNumbers(el) {
    if (isNaN(el)) return 'Аругмент должен быть числом, ';

    return el * 10 + ', ';
}

function usersInfo(user) {
    if (!user.age) return new Error('Укажите возраст пользователя по имени ' + user.name + ', ');
    if (!user.name) return new Error('Укажите имя пользователя с возрастом' + user.age + ', ');

    return user.name + ' is ' + user.age + ', ';
}

function invertStrings(el) {
    if (el === undefined ) return new Error('Не передана строка');
    if (typeof(el) !== 'string') return new Error('Аргумент не является строкой');

    let res = '';

    for (let i = el.length - 1; i >= 0; i--) {
        res += el[i];
    }

    return res + ', ';
}

console.log(showMessage(['my', 'name', 'is', 'Trinity'], uppercaseFirstLetter)); // New value: MyNameIsTrinity
console.log(showMessage(['10', '20', '30'], increaseNumbers)); // New value: 100, 200, 300,
console.log(showMessage([{age: 45, name: 'Jhon'}, {age: 20, name: 'Aaron'}], usersInfo)); // New value: Jhon is 45, Aaron is 20,
console.log(showMessage(['abc', '123'], invertStrings)); // New value: cba, 321


// Рекурсия (разобраться и сделать позже)

// --------------------------------------------------------------------------------------------------------------
// 2. Факториал числа - произведение всех натуральных чисел от 1 до n включительно: 3! = 3*2*1, 5! = 5*4*3*2*1.
//    С помощью рекурсии вычислить факториал числа 10: factorial(10) = 3628800
// --------------------------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------------------------
// 5. Написать функцию-рекурсию, которая выведет каждый символ строки в консоль: printChars('test')
//                                  // "t"
//                                  // "e"
//                                  // "s"
//                                  // "t"
// --------------------------------------------------------------------------------------------------------------


// 7. Function every
function every(arr, handler) {
    for (let i = 0; i < arr.length; i++) {
        if (!handler(arr[i])) {
            return false;
        }
    }

    return false;
}

let customEvery = every([12312313, 3252, 1, 54, 2345, 'abc'], function(el){
    return typeof el === 'number';
});

console.log(customEvery); // false
